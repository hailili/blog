
app_root = '/var/www/lhlblog/current'
pidfile "#{app_root}/tmp/pids/puma.pid"
state_path "#{app_root}/tmp/pids/puma.state"
stdout_redirect "#{app_root}/log/puma.stdout.log", "#{app_root}/log/puma.stderr.log", true
bind 'unix:/tmp/unicorn.ruby-china.sock'
daemonize true
port 7000
workers 4
threads 8, 16
preload_app!


