require "bundler/capistrano"   #部署时运行bundle
set :user, 'menglv'            #存放web的用户名
set :domain, '52.197.209.24'        #服务器IP
set :application, 'lhlblog'    #项目文件名，既将会在user里面创建这个名字的文件夹
# adjust if you are using RVM, remove if you are not

# file paths
set :scm, 'git'
set :rep_url, "git@bitbucket.org:hailili/blog.git"  #git所存在的地址，我存放在自己的服务器中
set :deploy_to, "/var/www/lhlblog"  #部署的地址
# distribute your applications across servers (the instructions below put them
# all on the same server, defined above as 'domain', adjust as necessary)


# miscellaneous options

namespace :deploy do
  after :finishing, 'deploy:cleanup'
end